# Contexte

On retrouve plusieurs notions dans Argo CD, mais la principale est la notion d'**Application**.

Une application est un "groupe de ressources kubernetes"
C'est un objet qui contient :

- une référence vers où trouver les descripteurs kubernetes.
- une référence vers où déployer cela.
- et un peu de configuration.

## Usage simple

On peut créer une apps soit via la ligne de commande, soit via un descripteur (à privilégier...).

exemple :

```shell
argocd app create hello-world --repo https://gitlab.com/gitops-heros/deploy-sith-from-gitlab.git --path 01-static-yaml --dest-server https://kubernetes.default.svc --sync-option CreateNamespace=true --dest-namespace test
```

> ⚠️ l'option `--sync-option CreateNamespace=true ` n'est pas fiable...  
> et il n'y a pas toutes les options sur la `syncPolicy`

Ou via un descripteur qui sera déployé via une commande `kubectl apply -f MON_DESCRIPTEUR_D-APP.YAML`

```yaml
---
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: hello-world
  namespace: argocd
spec:
  project: default
  destination:
    namespace: test
    server: https://kubernetes.default.svc
  source:
    path: 01-static-yaml
    repoURL: https://gitlab.com/gitops-heros/deploy-sith-from-gitlab.git
    targetRevision: main
  syncPolicy:
    automated:
      prune: true
      selfHeal: true
    syncOptions:
      - CreateNamespace=true
```

Vous déployez les descripteurs d'`Applications ArgoCD` dans votre clusteur kubernetes. Ce sont donc des commandes à passer **depuis votre workspace gipods `workshopadventure`.**

Cette apps va déployer dans le serveur où est déployé Argo CD (https://kubernetes.default.svc est l'url interne de l'API server) les manifests kubernetes qui sont dans le repository publique https://gitlab.com/gitops-heros/deploy-sith-from-gitlab.git (on peut gérer des repository privés, mais c'est une autre histoire), dans le répertoire `01-static-yaml` et sur la branche `main`.
Elle doit aussi créer le namespace s'il n'existe pas. Si l'on modifie l'application sans passer par une modification du repository, ArgoCD doit revenir automatiquement à état désirée.

> ⚠️ Si l'application est supprimée les ressources sont elles aussi supprimées (à part le namespace, sauf s'il fait partie des descripteurs).

Vous trouverez les modèles d'applications ArgoCD dans le dossier `argo-apps` (et dans les sous dossiers vous aurez les définitions spécifiques à chaque type de déployement utilisés). Vous pouvez utiliser ces descripteurs mais sachez que pour ceux de "production" il reste des choses à faire.

```
argo-apps/
├── dhall
├── helm
└── kustomize
```

### Notes

- Les `Applications` ArgoCD décrivent où trouver les composants kubernetes. La configuration de ces composants (exemple: version de l'image, nombre d'instances, ...) est gérée directement dans le repository qui contient ces composants kubernetes (et pas l'`Application` ArgoCD).
- Parmis les concepts plus avancés (qui ne seront pas abordés) il y en a deux qui sont intéressants de regarder : l'Apps de Apps et les AppSet.
